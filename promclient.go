package main

import (
	"io/ioutil"
	"net/http"
)

// Define struct for Prometheus config.

func promclient(config PrometheusConfig) []byte {

	// Timeout for the HTTP requests to Prometheus
	promHttpTimeout := config.promHttpTimeout

	// Hostname for the Prometheus instance
	promHost := config.promHost

	// Port for the Prometheus HTTP API. Default is 9090
	promPort := config.promPort

	// Prometheus Query to execute. The HTTP API is documented here. https://prometheus.io/docs/prometheus/latest/querying/api/
	promQuery := config.promQuery

	//Create HTTP Client to query Prometheus and read response into a bytearray.
	promClient := http.Client{Timeout: promHttpTimeout}
	response, httpErr := promClient.Get("http://" + promHost + ":" + promPort + promQuery)
	checkErr(httpErr)
	var responseBytes, readAllErr = ioutil.ReadAll(response.Body)
	checkErr(readAllErr)
	checkErr(response.Body.Close())
	return responseBytes
}
