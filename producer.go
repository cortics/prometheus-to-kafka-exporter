// This program executes a query on a Prometheus database and writes the result to a Kafka topic.
package main

import (
	"context"
	"fmt"
	"github.com/segmentio/kafka-go"
	"time"
)

func producer(config ProducerConfig) {

	// List of brokers in the Kafka cluster
	kafkaHost := config.kafkaHost

	//Kafka Port. Default is 9092 for plaintext, 9093 for SSL.
	kafkaPort := config.kafkaPort

	// Name of the Kafka topic to produce messages.
	kafkaTopic := config.kafkaTopic

	// Master key for encrypting messages. Should be a 32 character string containing only "[a-zA-Z0-9]"
	masterKey := config.masterKey
	fmt.Println(masterKey)

	//Message placeholder
	message := make([]byte, 0)

	// List of producer parameters. Defaults work in most cases.
	maxAttempts := config.maxAttempts
	queueCapacity := config.queueCapacity
	batchSize := config.batchSize
	batchTimeout := config.batchTimeout
	readTimeout := config.readTimeout
	writeTimeout := config.writeTimeout
	rebalanceInterval := config.rebalanceInterval
	requiredAcks := config.requiredAcks
	async := config.async

	// Generate data encryption key and use it to encrypt the response.
	dataKey := randomString(32)
	dataNonce := time.Now().UnixNano()
	messageKey := []byte(dataKey + "|" + string(dataNonce))
	messageValue := Encrypt([]byte(dataKey), message, []byte(string(dataNonce)))

	//Create Kafka producer client and write data to given Kafka cluster and topic.
	writerConfig := kafka.WriterConfig{
		Brokers:           []string{kafkaHost + ":" + kafkaPort},
		Topic:             kafkaTopic,
		MaxAttempts:       maxAttempts,
		QueueCapacity:     queueCapacity,
		BatchSize:         batchSize,
		BatchTimeout:      batchTimeout,
		ReadTimeout:       readTimeout,
		WriteTimeout:      writeTimeout,
		RebalanceInterval: rebalanceInterval,
		RequiredAcks:      requiredAcks,
		Async:             async,
	}
	topicWriter := kafka.NewWriter(writerConfig)
	checkErr(topicWriter.WriteMessages(context.Background(), kafka.Message{Key: messageKey, Value: messageValue}))
	//checkErr(topicWriter.WriteMessages(context.Background(), kafka.Message{Key: []byte(time.Now().UTC().String()), Value: responseBytes}))
	checkErr(topicWriter.Close())
}
