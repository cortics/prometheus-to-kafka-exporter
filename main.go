// This program executes a query on a Prometheus database and writes the result to a Kafka topic.
package main

import (
	"github.com/spf13/viper"
	"time"
)

func main() {

	// Setup viper to read the config file.
	viper.SetConfigName("config")
	viper.AddConfigPath(".")
	viper.AddConfigPath("/etc/prometheus-kafka-exporter/")
	viper.AddConfigPath("/opt/prometheus-kafka-exporter/")
	checkErr(viper.ReadInConfig())

	// Get sample rate from config.
	sampleRate := viper.GetDuration("sample-rate")

	// Read values from config file into ProducerConfig Object.
	producerConfig := ProducerConfig{
		kafkaHost:         viper.GetString("kafka-host"),
		kafkaPort:         viper.GetString("kafka-port"),
		kafkaTopic:        viper.GetString("kafka-topic"),
		masterKey:         viper.GetString("master-key"),
		maxAttempts:       viper.GetInt("producer-max-attempts"),
		queueCapacity:     viper.GetInt("producer-queue-capacity"),
		batchSize:         viper.GetInt("producer-batch-size"),
		batchTimeout:      viper.GetDuration("producer-batch-timeout"),
		readTimeout:       viper.GetDuration("producer-read-timeout"),
		writeTimeout:      viper.GetDuration("producer-write-timeout"),
		rebalanceInterval: viper.GetDuration("producer-rebalance-interval"),
		requiredAcks:      viper.GetInt("producer-required-acks"),
		async:             viper.GetBool("producer-async"),
	}

	// Read values from config file into ConsumerConfig Object.
	consumerConfig := ConsumerConfig{
		kafkaHost:       viper.GetString("kafka-host"),
		kafkaPort:       viper.GetString("kafka-port"),
		kafkaTopic:      viper.GetString("kafka-topic"),
		kafkaPartition:  viper.GetInt("consumer-partition"),
		startOffset:     viper.GetInt64("consumer-offset"),
		masterKey:       viper.GetString("master-key"),
		queueCapacity:   viper.GetInt("consumer-queue-capacity"),
		minBytes:        viper.GetInt("consumer-min-bytes"),
		maxBytes:        viper.GetInt("consumer-max-bytes"),
		maxWait:         viper.GetDuration("consumer-max-wait"),
		readLagInterval: viper.GetDuration("consumer-read-lag-interval"),
	}

	// Read values from config file into PrometheusConfig Object.
	prometheusConfig := PrometheusConfig{
		promHttpTimeout: viper.GetDuration("prometheus-http-timeout"),
		promHost:        viper.GetString("prometheus-host"),
		promPort:        viper.GetString("prometheus-port"),
		promQuery:       viper.GetString("prometheus-query"),
	}

	// Setup an infinite loop based on the sample rate provided.
	ticker := time.NewTicker(sampleRate)
	for range ticker.C {
		promclient(prometheusConfig)
		producer(producerConfig)
		consumer(consumerConfig)
	}
}
