package main

import "time"

// Define struct for Producer config.
type ProducerConfig struct {
	kafkaHost         string
	kafkaPort         string
	kafkaTopic        string
	masterKey         string
	maxAttempts       int
	queueCapacity     int
	batchSize         int
	batchTimeout      time.Duration
	readTimeout       time.Duration
	writeTimeout      time.Duration
	rebalanceInterval time.Duration
	requiredAcks      int
	async             bool
}

// Define struct for Consumer config.
type ConsumerConfig struct {
	kafkaHost       string
	kafkaPort       string
	kafkaTopic      string
	masterKey       string
	kafkaPartition  int
	startOffset     int64
	queueCapacity   int
	minBytes        int
	maxBytes        int
	maxWait         time.Duration
	readLagInterval time.Duration
}

// Define struct for Prometheus config.
type PrometheusConfig struct {
	promHttpTimeout time.Duration
	promHost        string
	promPort        string
	promQuery       string
}
