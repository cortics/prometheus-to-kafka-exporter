package main

import (
	"fmt"
	"os"
)

// This function is a catch-all for non-retriable errors that may occur in a program.
// Examples include File I/O, HTTP requests, database calls and almost all kinds of RPCs.
func checkErr(err error) {
	if err != nil {
		fmt.Println("Fatal error! Exiting... ", err.Error())
		os.Exit(1)
	}
}
