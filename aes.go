package main

import (
	"crypto/aes"
	"crypto/cipher"
	"crypto/rand"
	"io"
)

func Encrypt(key []byte, plainText []byte, nonce []byte) []byte {

	block, cipherErr := aes.NewCipher(key)
	checkErr(cipherErr)

	_, readErr := io.ReadFull(rand.Reader, nonce)
	checkErr(readErr)

	aesGcm, gcmErr := cipher.NewGCM(block)
	checkErr(gcmErr)

	cipherText := aesGcm.Seal(nil, nonce, plainText, nil)
	return cipherText
}

func Decrypt(key []byte, cipherText []byte, nonce []byte) []byte {
	block, cipherErr := aes.NewCipher(key)
	checkErr(cipherErr)

	aesGcm, gcmErr := cipher.NewGCM(block)
	checkErr(gcmErr)

	plainText, openErr := aesGcm.Open(nil, nonce, cipherText, nil)
	checkErr(openErr)
	return plainText
}
