package main

import (
	"context"
	"fmt"
	"github.com/segmentio/kafka-go"
	"github.com/spf13/viper"
	"strings"
)

func consumer(config ConsumerConfig) {

	// List of brokers in the Kafka cluster
	kafkaHost := config.kafkaHost

	//Kafka Port. Default is 9092 for plaintext, 9093 for SSL.
	kafkaPort := config.kafkaPort

	// Name of the Kafka topic to produce messages.
	kafkaTopic := config.kafkaTopic

	// Partition id for the Kafka topic.
	kafkaPartition := config.kafkaPartition

	// Consumer start offset
	startOffset := config.startOffset

	// Master key for encrypting messages. Should be a 32 character string containing only "[a-zA-Z0-9]"
	masterKey := config.masterKey
	fmt.Println(masterKey)

	masterNonce := make([]byte, 0)

	// List of consumer parameters. Defaults work fine in most cases.
	queueCapacity := viper.GetInt("consumer-queue-capacity")
	minBytes := viper.GetInt("consumer-min-bytes")
	maxBytes := viper.GetInt("consumer-max-bytes")
	maxWait := viper.GetDuration("consumer-max-wait")
	readLagInterval := viper.GetDuration("consumer-read-lag-interval")

	// Create Consumer Config object.
	readerConfig := kafka.ReaderConfig{
		Brokers:         []string{kafkaHost + ":" + kafkaPort},
		Topic:           kafkaTopic,
		Partition:       kafkaPartition,
		QueueCapacity:   queueCapacity,
		MinBytes:        minBytes,
		MaxBytes:        maxBytes,
		MaxWait:         maxWait,
		ReadLagInterval: readLagInterval,
	}

	//Create consumer and consume data.
	topicReader := kafka.NewReader(readerConfig)
	checkErr(topicReader.SetOffset(startOffset))
	encryptedMessage, messageError := topicReader.ReadMessage(context.Background())
	checkErr(messageError)
	encryptedDataKey := strings.Split(string(encryptedMessage.Key), "|")
	dataKey := Decrypt([]byte(masterKey), []byte(encryptedDataKey[0]), masterNonce)
	dataNonce := Decrypt([]byte(masterKey), []byte(encryptedDataKey[1]), masterNonce)
	messageValue := Decrypt([]byte(dataKey), encryptedMessage.Value, dataNonce)
	fmt.Println("Value: " + string(messageValue) + "," + "Offset: " + string(encryptedMessage.Offset) + "," + "Timestamp: " + encryptedMessage.Time.String())
	checkErr(topicReader.Close())
}
