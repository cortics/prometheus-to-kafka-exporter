# prometheus-to-kafka-exporter  [![Go Report Card](https://goreportcard.com/badge/github.com/cortics/prometheus-to-kafka-exporter)](https://goreportcard.com/report/github.com/cortics/prometheus-to-kafka-exporter) [![License](https://img.shields.io/badge/License-Apache%202.0-blue.svg)](https://opensource.org/licenses/Apache-2.0) [![GoDoc](https://godoc.org/github.com/cortics/prometheus-to-kafka-exporter?status.svg)](https://godoc.org/github.com/cortics/prometheus-to-kafka-exporter)

This is a small program that executes a query against [Prometheus](https://prometheus.io/) and writes the result to a [Kafka](http://kafka.apache.org/) topic at regular configurable intervals. 

## Getting Started

You will need a functional Kafka cluster as well as Prometheus setup in order to use this program. 

You will need to install [Go](https://golang.org/) in order to build from source. You can get a one line install using  [Stiymi](https://github.com/cortics/stiymi).

You can get a Kafka sandbox using [Landoop's](https://github.com/Landoop) [fast-data-dev](https://github.com/Landoop/fast-data-dev) Docker image.

```bash
docker run -dit --name kafka-sbx -p 2181:2181 -p 3030:3030 -p 8081-8083:8081-8083 -p 9581-9585:9581-9585 -p 9092-9093:9092-9093 -e ADV_HOST=<your-machine-ip> -e ENABLE_SSL=1 landoop/fast-data-dev:latest 
```

You can get a Prometheus sandbox using their official Docker image.

```bash
docker run -dit -p 9090:9090 --name prom-sbx prom/prometheus
```

You can download the binary for macOS from the releases page. You will need to build from source for other platforms.

The config file template needs to be updated to reflect your Kafka cluster, prometheus server and related parameters. The program will look for `config.json` in the current directory, `/etc/kafka-to-prometheus-exporter/` and `/opt/kafka-to-prometheus-exporter`.

The Prometheus HTTP API docs are available [here](https://prometheus.io/docs/prometheus/latest/querying/api/)

Run the following commands to start the program.
```bash
chmod +x kafka-to-prometheus-exporter
./kafka-to-prometheus-exporter/
```

You should now see the query results as messages in the relevant Kafka topic. The keys are UTC timestamps when the message was created, the values are the responses from the HTTP requests.

## Building from source

```bash
git clone git@github.com:cortics/prometheus-to-kafka-exporter.git
cd prometheus-to-kafka-exporter/
go build
```
You should now have a binary in the project root directory.

## Contributing

Contributions are welcome and appreciated!

